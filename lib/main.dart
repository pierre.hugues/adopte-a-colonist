import 'dart:convert';

import 'package:create_a_colonist/screens/landing_screen.dart';
import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var themeData = ThemeData(
        primaryColor: Colors.black,
        accentColor: Colors.brown,
        fontFamily: "NotoSans",
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 16.0, color: Color(0xFF000000), fontWeight: FontWeight.bold),
          headline2: TextStyle(fontSize: 36.0, color: Color(0xFF593D28)),
          headline3: TextStyle(fontSize: 24.0, color: Color(0xFF000000)),
          headline4: TextStyle(fontSize: 16.0, color: Color(0xFF000000)),
          headline5: TextStyle(fontSize: 12.0, color: Color(0xFF000000)),
          headline6: TextStyle(fontSize: 12.0, color: Color(0xFF593D28)),
        ));

    return MaterialApp(
      title: 'Flutter Demo',
      theme: themeData,
      home: LandingScreen(),
    );
  }
}
