import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class ColonistComponent extends StatefulWidget {
  const ColonistComponent(
      {Key key, this.id, this.name, this.description, this.lvl, this.image})
      : super(key: key);
  final int id;
  final String name;
  final String description;
  final int lvl;
  final String image;

  @override
  _ColonistState createState() => _ColonistState();
}

class _ColonistState extends State<ColonistComponent> {
  bool _checkbox;
  var colonists;
  final LocalStorage storage = new LocalStorage('adopt_a_colonist');

  @override
  void initState() {
    super.initState();
    colonists = storage.getItem('colonists') ?? [];
    if (colonists.contains(widget.id)) {
      _checkbox = true;
    } else {
      _checkbox = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          border: Border.all(color: Colors.black),
        ),
        child: Row(
            children: [
              new Image(
                image: AssetImage(widget.image.toString()),
                height: 100,
                width: 120,
              ),
              Column(
                  children: [
                    Text(
                      widget.name,
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    Text(
                      widget.description,
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline5,
                    )
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center),
              Spacer(),
              Column(
                  children: [
                    Text(
                      "lvl " + widget.lvl.toString(),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    Checkbox(
                      value: _checkbox,
                      onChanged: (value) {
                        setState(() {
                          _checkbox = value;
                        });
                        if (value) {
                          colonists.add(widget.id);
                        } else {
                          colonists.remove(widget.id);
                        }
                        storage.setItem('colonists', colonists);
                      },
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center));
  }
}
