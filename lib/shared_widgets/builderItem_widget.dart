import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class BuilderComponent extends StatefulWidget {
  const BuilderComponent({Key key, this.id, this.name, this.description, this.lvl, this.image})
      : super(key: key);

  final int id;
  final String name;
  final String description;
  final int lvl;
  final String image;


  @override
  _BuilderState createState() => _BuilderState();
}

class _BuilderState extends State<BuilderComponent> {
  bool _checkbox;
  var colonists;
  final LocalStorage storage = new LocalStorage('adopt_a_colonist');

  @override
  void initState() {
    super.initState();
    colonists = storage.getItem('colonists') ?? [];
    if(colonists.contains(widget.id)) {
      _checkbox = true;
    } else {
      _checkbox = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              border: Border.all(color: Colors.black),
            ),
            child: Column(children: [
              new Image(
                image: AssetImage(widget.image),
                height: 120,
                width: 120,
              ),
              Row(
                children: [
                  Column(children: [
                    Padding(
                        padding: EdgeInsets.only(left: 5, top: 5),
                        child: Text(
                          widget.name,
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.headline4,
                        )),
                    Padding(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          widget.description,
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.headline5,
                        ))
                  ], crossAxisAlignment: CrossAxisAlignment.start),
                  Spacer(),
                  Padding(
                      padding: EdgeInsets.only(right: 5),
                      child: Text(
                        "lvl " + widget.lvl.toString(),
                        style: Theme.of(context).textTheme.headline6,
                      )),
                ],
              ),
              Checkbox(
                value: _checkbox,
                onChanged: (value) {
                  setState(() {
                    _checkbox = value;
                  });
                  if(value) {
                    colonists.add(widget.id);
                  } else {
                    colonists.remove(widget.id);
                  }
                  storage.setItem('colonists', colonists);
                },
              ),
            ]));
  }
}
