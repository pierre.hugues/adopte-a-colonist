import 'package:create_a_colonist/screens/teamList_screen.dart';
import 'package:flutter/material.dart';

class Header extends PreferredSize {
  final double height;
  final bool displayTeam;

  Header({this.height = kToolbarHeight, @required this.displayTeam});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: preferredSize.height,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/header.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Row(
              children: [
                ElevatedButton(
                    onPressed: () => Navigator.pop(context),
                    child: Icon(Icons.arrow_back),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.transparent),
                      shadowColor:
                          MaterialStateProperty.all<Color>(Colors.transparent),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.black),
                    )),
                displayTeam
                    ? Padding(
                        padding: EdgeInsets.only(
                            left: (MediaQuery.of(context).size.width - 128)),
                        child: ElevatedButton(
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TeamListScreen())),
                            child: Icon(Icons.group),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.transparent),
                              shadowColor: MaterialStateProperty.all<Color>(
                                  Colors.transparent),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.black),
                            )),
                      )
                    : Text(""),
              ],
            )));
  }
}
