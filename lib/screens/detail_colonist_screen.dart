import 'package:create_a_colonist/data/colonist_list.dart';
import 'package:flutter/material.dart';
import 'package:create_a_colonist/shared_widgets/header_widget.dart';

class DetailColonistScreen extends StatefulWidget {
  const DetailColonistScreen({Key key, this.id}) : super(key: key);

  final int id;

  @override
  _DetailColonistScreenState createState() => _DetailColonistScreenState();
}

class _DetailColonistScreenState extends State<DetailColonistScreen> {
  var colonistList = new ColonistList();
  Future<Map<String, dynamic>> colonists;

  @override
  void initState() {
    super.initState();
    colonists = getColonists();
  }

  Future<Map<String, dynamic>> getColonists() async {
    var colonists = await colonistList.getColonistLists();
    return colonists['colonists'][widget.id];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        displayTeam: false,
        height: 120,
      ),
      body: FutureBuilder<Map<String, dynamic>>(
        future: colonists,
        builder: (BuildContext context,
            AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                Column(
                  children: [
                    Image.asset(
                      snapshot.data['image_big'],
                      height: 200,
                      fit: BoxFit.fill,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: Text(snapshot.data['name'],
                        style: Theme.of(context).textTheme.headline3),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 15),
                      child: Text("lvl." + snapshot.data['level'].toString(),
                          style: Theme.of(context).textTheme.headline6)
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: Text(snapshot.data['message'],
                        style: Theme.of(context).textTheme.headline6),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                Table(
                  border: TableBorder.all(width: 1.0, color: Colors.black),
                  children: [
                    TableRow(children: [
                      TableCell(
                        child: Text("Skills",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline1,
                            ),
                      ),
                      TableCell(
                        child: Text("Level",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline1),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Athletics",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['athletics'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Dexterity",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['dexterity'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Strength",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['strength'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Mana",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['mana'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Adaptability",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['adaptability'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Agility",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['agility'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Focus",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['focus'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Creativity",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['creativity'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Knowledge",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['knowledge'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                    TableRow(children: [
                      TableCell(
                        child: Text("Intelligence",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                      TableCell(
                        child: Text(snapshot.data['intelligence'].toString(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    ]),
                  ],
                )
              ],
            );
          } else {
            return Text('Calculating answer...');
          }
        },
      ),
    );
  }
}
