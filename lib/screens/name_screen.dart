import 'package:flutter/material.dart';
import 'package:create_a_colonist/shared_widgets/header_widget.dart';

import 'builderList_screen.dart';

class NameScreen extends StatefulWidget {
  const NameScreen({Key key}) : super(key: key);

  @override
  _NameScreenState createState() => _NameScreenState();
}

class _NameScreenState extends State<NameScreen> {
  String playerName = "";
  String teamName = "";

  void _setPlayerName(String name) {
    setState(() {
      playerName = name;
    });
  }

  void _setTeamName(String team) {
    setState(() {
      teamName = team;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Header(
          displayTeam: false,
          height: 120,
        ),
        body: Container(
            margin:
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 40.0),
            child: Column(children: [
              Text(
                'What\'s your player name ?',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              Container(
                  margin: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 40.0),
                  child: Column(children: [
                    TextFormField(
                      decoration: const InputDecoration(
                        hintText: 'Enter your nickname',
                        labelText: 'Player name',
                      ),
                      onChanged: (text) {
                        _setPlayerName(text);
                      },
                      validator: (String value) {
                        return (value == null)
                            ? 'This field can\'t be empty.'
                            : null;
                      },
                    ),
                  ])),
              Container(
                margin: const EdgeInsets.symmetric(
                    horizontal: 25.0, vertical: 40.0),
                child: Column(children: [
                  Text('What\'s your team name ?',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Enter the name of your team',
                      labelText: 'Team name',
                    ),
                    onChanged: (text) {
                      _setTeamName(text);
                    },
                    validator: (String value) {
                      return (value == null)
                          ? 'This field can\'t be empty.'
                          : null;
                    },
                  ),
                ]),
              ),
              Text(
                playerName + '\'s team is ' + teamName,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xFF593D28)),
                      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0))),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ColonistListScreen()));
                  },
                  child: Text("Choose your colonists"),
                ),
              )
            ])));
  }
}
