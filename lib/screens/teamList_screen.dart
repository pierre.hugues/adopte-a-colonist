import 'package:create_a_colonist/data/colonist_list.dart';
import 'package:create_a_colonist/screens/ending_screen.dart';
import 'package:create_a_colonist/shared_widgets/colonist-teamItem_widget.dart';
import 'package:create_a_colonist/shared_widgets/header_widget.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class TeamListScreen extends StatefulWidget {
  @override
  _TeamListScreenState createState() => _TeamListScreenState();
}

class _TeamListScreenState extends State<TeamListScreen> {
  var colonistList = new ColonistList();
  var idcolonists;
  Future<List<Map<String, dynamic>>> colonists;
  final LocalStorage storage = new LocalStorage('adopt_a_colonist');

  @override
  void initState() {
    super.initState();
    colonists = getColonists();
    idcolonists = storage.getItem('colonists') ?? [];
  }

  Future<List<Map<String, dynamic>>> getColonists() async {
    var colonists = await colonistList.getColonistLists();
    return colonists['colonists'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        displayTeam: false,
        height: 120,
      ),
      body: FutureBuilder<List<Map<String, dynamic>>>(
          future: colonists,
          builder: (BuildContext context,
              AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (idcolonists.contains(index)) {
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: ColonistComponent(
                            id: snapshot.data[index]['id'],
                            name: snapshot.data[index]['name'],
                            description: snapshot.data[index]['message'],
                            lvl: snapshot.data[index]['level'],
                            image: snapshot.data[index]['image']),
                      );
                    } else {
                      return Padding(padding: EdgeInsets.zero);
                    }
                  });
            } else {
              return Text('Calculating answer...');
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => EndingScreen()));
            },
            tooltip: 'Choose your colonists',
            child: Icon(Icons.check),
        ), 
    );
  }
}
