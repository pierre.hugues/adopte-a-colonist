import 'package:create_a_colonist/shared_widgets/header_widget.dart';
import 'package:flutter/material.dart';

class EndingScreen extends StatelessWidget {
  const EndingScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        displayTeam: false,
        height: 120,
      ),
      body: Center(
        child: Column(
          children: [
            Image.asset("assets/images/minecraftEnding.png"),
            Padding(
              padding: EdgeInsets.all(30.0),
              child: Text("There you go !", style: Theme.of(context).textTheme.headline2)
            ),
            Text("Your team is ready", style: Theme.of(context).textTheme.headline3),
            Padding(
              padding: EdgeInsets.only(top: 100.0),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Color(0xFF593D28)),
                  padding:  MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0))
                ),
                onPressed: () { 
                  Navigator.popUntil(context, (Route<dynamic> predicate) => predicate.isFirst);
                }, 
                child: Text("Go home !"),
              ),
            )
          ],
        ),
      )
    );
  }
}