import 'package:create_a_colonist/data/colonist_list.dart';
import 'package:create_a_colonist/screens/detail_colonist_screen.dart';
import 'package:create_a_colonist/shared_widgets/builderItem_widget.dart';
import 'package:create_a_colonist/shared_widgets/header_widget.dart';
import 'package:flutter/material.dart';

class ColonistListScreen extends StatefulWidget {
  @override
  _ColonistListScreenState createState() => _ColonistListScreenState();
}

class _ColonistListScreenState extends State<ColonistListScreen> {
  var colonistList = new ColonistList();
  Future<List<Map<String, dynamic>>> colonists;

  @override
  void initState() {
    super.initState();
    colonists = getColonists();
  }

  Future<List<Map<String, dynamic>>> getColonists() async {
    var colonists = await colonistList.getColonistLists();
    return colonists['colonists'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        displayTeam: true,
        height: 120,
      ),
      body: FutureBuilder<List<Map<String, dynamic>>>(
        future: colonists,
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    DetailColonistScreen(id: index)));
                      },
                      child: ListTile(
                          title: BuilderComponent(
                              id: snapshot.data[index]['id'],
                              name: snapshot.data[index]['name'],
                              description: snapshot.data[index]['message'],
                              lvl: snapshot.data[index]['level'],
                              image: snapshot.data[index]['image'])));
                });
          } else {
            return Text('Couldn\'t retrieve json');
          }
        },
      ),
    );
  }
}
