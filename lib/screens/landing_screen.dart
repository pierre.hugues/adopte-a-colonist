import 'package:create_a_colonist/screens/name_screen.dart';
import 'package:flutter/material.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/minecraftBackground.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 200.0, left: 30.0, right: 30.0),
            child: Image.asset("assets/images/title.png"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 200.0),
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Color(0xFF62A138)),
                  padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0))),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NameScreen()));
              },
              child: Text("Create your team !"),
            ),
          )
        ],
      ),
    ));
  }
}
